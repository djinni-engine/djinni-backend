lint:
	flake8 *.py test models utils routes  --max-line-length=120

lint-format:
	black  --line-length=120 *.py test models utils routes 

unit-test:
	pytest test/

# experimental
setup-unit-test:
	make rm-unit-setup
	docker start test_db
	docker start test_redis
	# WARNING IF TESTING IS NOT SET TO TRUE THIS WILL modify PRODUCTION DB
	# TODO modify environment variable TESTING=true


# experimental #probably could use something like update instead of rm + kill
rm-unit-setup:
	docker stop test_db && docker stop test_redis

fixtures:
	# ./scripts/insert_fixture_books.py
	echo TODO

clear-db:
	./scripts/clear_db.py