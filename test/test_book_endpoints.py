import pytest
import asyncio
from starlette.testclient import TestClient
from models.book import Book
from run import app
from test.fixtures import queries
from utils.db_book_functions import clear_db


client = TestClient(app)
loop = asyncio.get_event_loop()

C1, C2 = queries["filter_twice"]["categories"]
V1, V2 = queries["filter_twice"]["values"]
INVALID_C = queries["invalid"]["c"]
INVALID_V = queries["invalid"]["v"]


def test_book_by_id(book1: Book):
    response = client.get(f"/v1/book?book_id={book1.book_id}")
    assert response.status_code == 200
    data = response.json()
    assert data['book'] == book1
    # loop.run_until_complete(clear_db())


def test_get_categories():
    response = client.get("/v1/book/categories")
    assert response.status_code == 200
    # data = response.json()['data']
    # TODO


def test_get_possible_values_of_category():
    response = client.get("/v1/book/hero")
    assert response.status_code == 200
    # data = response.json()['data']
    # TODO


def test_get_possible_values_of_category_invalid_category_name():
    response = client.get(f"/v1/book/{INVALID_C}")
    assert response.status_code == 422


def test_filter_for_multiple_category_values(book1: Book):
    response = client.post("/v1/book/filter/", json={"cs": [C1, C2], "vs": [V1, V2]})
    assert response.status_code == 200
    data = response.json()
    assert book1.title in str(data)  # HACKY


# TODO
@pytest.mark.skip()
def test_filters_order_is_recognized():
    pass


def test_filter_for_multiple_category_values_invalid_category_or_value():
    response = client.post("/v1/book/filter/", json={"cs": [INVALID_C, C2], "vs": [V1, V2]})
    assert response.status_code == 422

    response = client.post("/v1/book/filter/", json={"cs": [C1, C2], "vs": [INVALID_V, V2]})
    assert response.status_code == 400


@pytest.mark.skip()
def test_post_book():
    pass


@pytest.mark.skip()
def test_post_book_invalid_format():
    pass


@pytest.mark.skip()
def test_post_book_duplicate_id():
    pass
