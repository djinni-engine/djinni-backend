# fixtures to be used during testing
# Do not assume that they will be in the database

b_id_1 = "af37116eb80a11ea9d638bc720ec67fd"
b_id_2 = "bf37116eb80a11ea9d638bc720ec66fd"
b_id_3 = "cf37116eb80a11ea9d638bc720ec66fd"
b_id_4 = "df37116eb80a11ea9d638bc720ec66fd"
b_id_5 = "ff37116eb80a11ea9d638bc720ec66fd"
book_ids = [b_id_1, b_id_2, b_id_3, b_id_4, b_id_5]

queries = {
    "filter_twice": {
        "categories": ["hero", "kick"],
        "values": ["dog", "real"],
        "result": {"book_id": b_id_1, "title": "call of the wild"},
    },
    "invalid": {"c": "cat", "v": "violence"},
    "valid": {"c": "hero", "v": "dog"},
}

users = {
    1: {"user_id": 1, "username": "user1", "password": "pass1", "role": "admin"},
    2: {"user_id": 2, "username": "user2", "password": "pass2", "role": "personal"},
}

books = {
    b_id_1: {
        "book_id": b_id_1,
        "title": "call of the wild",
        "kick": "real",
        "hero": "dog",
        "duration": "short",
        "language": "english",
    },
    b_id_2: {
        "book_id": b_id_2,
        "title": "too like the lightning",
        "kick": "brainy",
        "hero": "human",
        "duration": "epic",
        "language": "english",
    },
    b_id_3: {
        "book_id": b_id_3,
        "title": "Surely you re joking Mr, Feynman",
        "kick": "funny",
        "hero": "human",
        "duration": "short",
        "language": "english",
    },
    b_id_4: {
        "book_id": b_id_4,
        "title": "35kg d espoir",
        "kick": "funny",
        "hero": "child",
        "duration": "short",
        "language": "french",
    },
    b_id_5: {
        "book_id": b_id_5,
        "title": "Die Abenteuer des Rox Schulz",
        "kick": "real",
        "hero": "human",
        "duration": "medium",
        "language": "german",
    },
}
