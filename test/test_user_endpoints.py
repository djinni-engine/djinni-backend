import asyncio
from starlette.testclient import TestClient
from run import app
import pytest
from test.fixtures import users, books, book_ids

client = TestClient(app)
loop = asyncio.get_event_loop()

USER1 = users[1]["username"]
PASS1 = users[1]["password"]


@pytest.mark.skip()
def test_user_register():
    pass


@pytest.mark.skip()
def test_user_data_validation_wrong_email():
    pass


@pytest.mark.skip()
def test_user_data_validation_duplicate_username():
    pass


@pytest.mark.skip()
def test_user_login():
    pass


@pytest.mark.skip()
def test_user_login_wrong_credentials():
    pass


def test_get_jwt_token_known_user():
    response = client.post("/v1/token", dict(username=USER1, password=PASS1))
    assert response.status_code == 200
    data = response.json()
    assert "access_token" in data


def test_get_jwt_token_unknown_user():
    response = client.post("/v1/token", dict(username="Superman", password=PASS1))
    assert response.status_code == 401


def test_jwt_required_valid_token():
    auth_header = get_auth_header()
    book = books[book_ids[0]]
    response = client.post("/v1/book", json=book, headers=auth_header)
    assert response.status_code == 201
    # TODO check it's really in the database
    # TODO clear db


def test_jwt_required_invalid_token():
    book = books[book_ids[0]]
    response = client.post("/v1/book", json=book, headers={"Authorization": "Bearer deadBeaf"})
    assert response.status_code == 401


# ========================================================================
# ===================== Helpers ==========================================
# ========================================================================
def get_auth_header():
    # insert_user("test", "test")
    # TODO
    response = client.post("/v1/token", dict(username=USER1, password=PASS1))
    jwt_token = response.json()["access_token"]
    auth_header = {"Authorization": f"Bearer {jwt_token}"}
    return auth_header
