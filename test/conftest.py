import pytest
from .fixtures import books, book_ids
import asyncio
from utils.db_object import db, activate_test_db, deactivate_test_db
from utils.db_book_functions import db_insert_book, db_remove_book
from models.book import Book
import utils.redis_object as re
from utils.redis_object import activate_test_redis, deactivate_test_redis

loop = asyncio.get_event_loop()


# REFACTOR find a way to connect to test_db if running tests
@pytest.fixture(scope="session", autouse=True)
def connect_test_db():
    """ connects the test DB before all tests are run and disconnects after """

    print("=========================== CONNECT")
    # loop.run_until_complete(activate_test_db())
    loop.run_until_complete(db.connect())
    loop.run_until_complete(activate_test_redis())


    yield

    print("=========================== DISCONNECT")
    # TODO ?? WHY does my deactivate db does not work??/
    # loop.run_until_complete(deactivate_test_db())
    loop.run_until_complete(db.disconnect())
    loop.run_until_complete(deactivate_test_redis())


@pytest.fixture(scope="function")
def book1():
    """ adds book1 to the database and removes it after test completion"""
    b1 = Book(**books[book_ids[0]])
    loop.run_until_complete(db_remove_book(b1.book_id))
    loop.run_until_complete(db_insert_book(b1))
    yield b1
    loop.run_until_complete(db_remove_book(b1.book_id))
