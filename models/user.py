from pydantic import BaseModel
from enum import Enum
from fastapi import Query
from typing import List


class Role(str, Enum):
    admin: str = "admin"
    personal: str = "personal"


class User(BaseModel):
    """ class to be saved in database (password in hashed form) """

    name: str
    password: str
    mail: str = Query(..., regex="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$")  # noqa: W605
    role: Role


class JWTUser(BaseModel):
    """ class to represent users when interacting with the endpoints """

    username: str
    password: str
    disabled: bool = False
    permissions: List[str] = []
