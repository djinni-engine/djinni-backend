from models.book import ALL_CATEGORIES_NAMES, CATEGORY_VALUE_MAP


def valid_category(c: str):
    return c in ALL_CATEGORIES_NAMES


def valid_value(c: str, v: str):
    return valid_category(c) and v in CATEGORY_VALUE_MAP[c]


# print(ALL_CATEGORIES[0].__members__)
# print(valid_value(ALL_CATEGORIES[0], "human"))
# print(valid_value(ALL_CATEGORIES[0], "cat"))
