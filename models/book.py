from pydantic import BaseModel
from enum import Enum


class Category(str, Enum):
    language: str = "language"
    hero: str = "hero"
    kick: str = "kick"
    duration: str = "duration"


# TODO REFACTOR: these could be classes
class Language(str, Enum):
    english: str = "english"
    german: str = "german"
    french: str = "french"


class Hero(str, Enum):
    human: str = "human"
    dog: str = "dog"
    child: str = "child"


class Kick(str, Enum):
    character_development: str = "character_development"
    brainy: str = "brainy"
    funny: str = "funny"
    real: str = "real"


class Duration(str, Enum):
    short: str = "short"
    medium: str = "medium"
    longer: str = "longer"
    epic: str = "epic"


class Book(BaseModel):
    book_id: str
    title: str
    # addedBy: str
    # tags: List[str]
    kick: Kick
    hero: Hero
    duration: Duration
    language: Language


ALL_CATEGORIES_NAMES = [e.value for e in Category]
ALL_CATEGORIES = [Hero, Kick, Language, Duration]

# TODO REFACTOR: this can be more elegant
CATEGORY_VALUE_MAP = {
    "kick": list(Kick.__members__.keys()),
    "hero": list(Hero.__members__.keys()),
    "language": list(Language.__members__.keys()),
    "duration": list(Duration.__members__.keys()),
}

# print(ALL_CATEGORIES_NAMES)
