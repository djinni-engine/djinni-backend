from fastapi import HTTPException, Body, Depends, APIRouter, FastAPI, Query
from models.book import (
    Book,
    Category,
    ALL_CATEGORIES_NAMES,
    CATEGORY_VALUE_MAP,
)
from models.user import User, JWTUser
from typing import List, Tuple
from starlette.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_409_CONFLICT,
    HTTP_404_NOT_FOUND,
    HTTP_201_CREATED,
    HTTP_401_UNAUTHORIZED,
)
from models.validate import valid_value
from utils.const import BAD_VALUE, FILTER_DESCRIPTION
from utils.util import flatten, FilterResult, BookAndDebug
from utils.db_book_functions import (
    db_get_book_by_id,
    db_get_all_books_from_category_value,
    db_filter_by_categories_and_values,
    db_get_options_after_filter,
    db_insert_book,
    db_insert_fixtures,
)
from fastapi.security import OAuth2PasswordRequestForm
from utils.security import authenticate_user, create_jwt_token, check_jwt_token
import utils.redis_object as re
import pickle

app_v1 = APIRouter()
# app_v1 = FastAPI(openapi_prefix="/v1")


# ========================================================================
# ======== INFO on possible categories and values or books ===============
# ========================================================================
@app_v1.get("/book/categories", tags=["overview"])
async def get_book_categories():
    return {"data": ALL_CATEGORIES_NAMES}


@app_v1.get("/book/{category}", tags=["overview"])
async def get_possible_category_values(category: Category):
    return {"data": CATEGORY_VALUE_MAP[category]}


@app_v1.get("/book", response_model=BookAndDebug, tags=["overview"])
async def get_book_by_id(book_id: str):
    redis_key = f"{book_id}"
    result = await re.redis.get(redis_key)
    # result = False
    if result:
        # pass
        book = pickle.loads(result)
        return {"loginfo": "from redis", "book": book}

    else:
        book_dict = await db_get_book_by_id(book_id)
        if book_dict:
            book = Book(**book_dict)
            await re.redis.set(redis_key, pickle.dumps(book), expire=30)
            return {"loginfo": "from db", "book": book}
        else:
            raise HTTPException(HTTP_404_NOT_FOUND, detail="Unknown book_id")


# ========================================================================
# ================= FILTER: actual books for given filter values ========
# ========================================================================
@app_v1.get("/book/filter/{category}/{value}", response_model=List[Book], tags=["filter"])
async def get_books_for_category_value(category: Category, value: str):
    if not valid_value(category, value):
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=BAD_VALUE)

    # redis caching:
    redis_key = f"filter:{category}:{value}"
    result = await re.redis.get(redis_key)
    if result:
        books = pickle.loads(result)
        return books
    else:
        book_dicts = await db_get_all_books_from_category_value(category, value)
        books = [Book(**d) for d in book_dicts]
        await re.redis.set(redis_key, pickle.dumps(books))

    return books


@app_v1.post("/book/filter/", description=FILTER_DESCRIPTION, response_model=FilterResult, tags=["filter"])
async def get_book_filtered_by_categories_and_value(
    cs: List[Category] = Body(..., example=["hero", "kick"], embed=True),
    vs: List[str] = Body(..., example=["dog", "real"], embed=True),
):
    """ filter for multiple categories and values, as indicated by order in parameters """
    print('got a nested filter request for', cs, vs)
    for cat, val in zip(cs, vs):
        if not valid_value(cat, val):
            raise HTTPException(HTTP_400_BAD_REQUEST, detail=f"{val} is no valid value of {cat}")

    # get list titles
    book_dict = await db_filter_by_categories_and_values(cs[::-1], vs[::-1])
    book_titles = flatten([d.values() for d in book_dict])

    options = await db_get_options_after_filter(cs[::-1], vs[::-1])
    # return {'books': 1, 'options': options}
    return FilterResult(books=book_titles, options=options)


# ========================================================================
# ===================== POST BOOKS =======================================
# ========================================================================
@app_v1.post("/book", status_code=HTTP_201_CREATED, tags=["store"])
async def post_book(
    book: Book = Body(
        ...,
        example={
            "book_id": "ff37116eb80a11ea9d638bc720ec66fd",
            "title": "My new book",
            "kick": "funny",
            "hero": "human",
            "duration": "short",
            "language": "french",
        },
    ),
    jwt: bool = Depends(check_jwt_token),
):
    alreadyIn = await db_get_book_by_id(book.book_id)
    if not alreadyIn:
        await db_insert_book(book)
        return HTTP_201_CREATED, "Success"
    else:
        raise HTTPException(HTTP_409_CONFLICT, "Book already exists")


@app_v1.post("/book/dev/fixtures/", status_code=HTTP_201_CREATED, tags=["store"])
async def insert_fixtures_into_db():
    added = await db_insert_fixtures()
    return {"added": added}


# ========================================================================
# =====================USER STUFF ========================================
# ========================================================================
# @app_v1.post("/user", status_code=HTTP_201_CREATED)
# async def post_user(user: User):
#     return {"request body": user}


# @app_v1.get("/user")
# async def get_user_validation(password: str):
#     return {"pw": "TODO"}


@app_v1.post("/token")
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    jwt_user_candidate = JWTUser(username=form_data.username, password=form_data.password)
    db_jwt_user = await authenticate_user(jwt_user_candidate)
    if not db_jwt_user:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="invalid username or password")

    jwt_token = create_jwt_token(db_jwt_user)
    return {"access_token": jwt_token}


# TODO store user in userDB (after signing up)
# @app_v1.post("/user")
# async def post_user(user: User):
#     return {'request body': user}
