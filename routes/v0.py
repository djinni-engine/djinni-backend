from fastapi import FastAPI

app_v0 = FastAPI(openapi_prefix="/v0")


@app_v0.get("/hello")
async def hello():
    return {"hello_there v0"}
