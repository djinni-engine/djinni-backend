## 1) Dependencies


fastapi pydantic uvicorn asyncio requests pytest databases uuid flake8 black mypy pyjwt passlib bcrypt aioredis locustio


> pip3 install fastapi
> pip3 install uvicorn
> ...

just see all that is in 'requirements.txt**

TODO: pin versions and organize it into a requirement.txt such that
> pip3 install -r requirements.txt

## 2) SETUP DB and cache layer 

**assumes docker installed**

#### DEV 

db on 5432:
> docker run --name=djinni -e POSTGRES_USER=admin -e POSTGRES_PASSWORD=admin -e POSTGRES_DB=djinni -p 5432:5432 -d postgres:10

cache on 
> docker run --name djinni-redis -p 6379:6379 -d redis


#### TEST test-db and test-redis

test_redis 6380:
> docker run --name test_redis -p 6380:6379 -d redis

run test_db on 5433

docker run --name=test_db -e POSTGRES_USER=test -e POSTGRES_PASSWORD=test -e POSTGRES_DB=test_db -p 5433:5432 -d postgres:1

#### PRODCUTION
TODO, basically the same as dev, ut on digital ocean instance

## RUN 
in root:

> uvicorn run:app --root-path /app --reload --port 3000


## DOCUMENTATION 

interact via swagger DOCS 

http://127.0.0.1:3000/docs#/

## TEST

when venv is active:  (maybe not needed?)
> source bsource venv/bin/activate

> make setup-unit-tests && make unit-tests
or
> pytest test/<pathtofile>::testNamewithoutParens

## WORKLOG NOTES, TROUBLESHOOTING

- create secret for jwt-tokens const.js (should be an env-variable later on)
>  openssl rand -hex 32 


check out the worklog.org file for helpful but scrambled notes



