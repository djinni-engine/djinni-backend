from fastapi import FastAPI, Depends
from routes.v1 import app_v1
from starlette.requests import Request
from datetime import datetime
from utils.db_object import db
import utils.redis_object as re
import aioredis
from utils.const import REDIS_URL, REDIS_URL_PRODUCTION, IS_PRODUCTION, TESTING
# from utils.redis_object import check_test_redis

app = FastAPI(
    title="Djinni Recommendation Engine Backend",
    description="Helps you know what you friends recommend to you",
    version="0.0.1",
)

app.include_router(
    app_v1, prefix="/v1" # dependencies=[Depends(check_test_redis)],
    # app_v1, prefix="/v1"
)

# app.mount("/v0", app_v0)
# app.mount("/v1", app_v1)


@app.on_event("startup")
async def connect_db():
    print("---------------------STARTING UP=============")
    await db.connect()
    re.redis = await aioredis.create_redis_pool(REDIS_URL_PRODUCTION if IS_PRODUCTION else REDIS_URL)


@app.on_event("shutdown")
async def disconnect_db():
    print("===================== SHUTTING DOWN ------------")
    await db.disconnect()

    re.redis.close()
    await re.redis.wait_closed()


@app.get("/")
async def health_check():
    return {"OK"}


@app.middleware("http")
async def middleware(request: Request, call_next):
    start_time = datetime.utcnow()
    # modify request

    # modify response
    response = await call_next(request)
    execution_time = (datetime.utcnow() - start_time).microseconds
    response.headers["x-execution-time"] = str(execution_time)
    return response
