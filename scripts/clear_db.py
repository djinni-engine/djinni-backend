#!/usr/bin/env python
import asyncio
from utils.db_book_functions import clear_db

loop = asyncio.get_event_loop()
loop.run_until_complete(clear_db())

# REFACTOR this should really be a command line tool