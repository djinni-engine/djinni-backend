#!/usr/bin/env python
import asyncio
from utils.db_book_functions import db_insert_fixtures

loop = asyncio.get_event_loop()

loop.run_until_complete(db_insert_fixtures())