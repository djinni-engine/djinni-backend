from utils.db import fetch
from models.user import JWTUser


async def db_check_token_user(user: JWTUser):
    """ returns all users with a given user is indeed in our DB """
    query = f"select * from users where username = '{user.username}'"
    result = await fetch(query, False, {})
    if result is None:
        return None
    else:
        return result


async def db_check_jwt_username(username: str) -> bool:
    """ True if a username exists """
    query = f"select * from users where username = '{username}'"
    result = await fetch(query, True, {})
    if result is None:
        return False
    else:
        return True
