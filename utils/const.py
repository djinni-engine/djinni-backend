from models.book import ALL_CATEGORIES_NAMES
import os

JWT_SECRET_KEY = "a301a4185572d1a83ea020629f65478f550683039918a19893ea58e8a0496eef"
JWT_ALGORITHM = "HS256"
JWT_EXPIRATION_TIME_MINUTES = 60 * 24 * 3

BAD_CATEGORY = f"The category you chose does not exist. Choose one of {ALL_CATEGORIES_NAMES}"
BAD_VALUE = "The value you chose does not exist in that category"

FILTER_DESCRIPTION = "Categories and values after which books should be filtered. \
                      Filters are being applied from left to right. \
                      NOTE: Pay attention that the values must be from the corresponding category."

ACCESS_PERMISSION = "ACCESS_PERMISSION"
DEV_PERMISSION = "DEV_PERMISSION"

JWT_EXPIRED_MSG = "You're JWT has expired. Please get a new one at /token"
JWT_INVALID = "Ooops, Invalid JWT Token!"
JWT_PERMISSIONS = "You don't have permissions to do that!"

# DB_HOST = "localhost:5433" # TODO 
DB_HOST = "localhost"
DB_USER = "admin"
DB_PASSWORD = "admin"
DB_NAME = "djinni"
DB_URL = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}"

DB_HOST_PRODUCTION = "10.110.0.2"
DB_URL_PRODUCTION = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST_PRODUCTION}/{DB_NAME}"

TEST_DB_HOST = "localhost:5433"
TEST_DB_USER = "test"
TEST_DB_PASSWORD = "test"
TEST_DB_NAME = "test_db"
TEST_DB_URL = f"postgresql://{TEST_DB_USER}:{TEST_DB_PASSWORD}@{TEST_DB_HOST}/{TEST_DB_NAME}"

TESTING = False
# TESTING = True
IS_PRODUCTION = True if os.getenv("PRODUCTION")=='true' else False

# usual port is 6379
TEST_REDIS_URL = "localhost:6380"
REDIS_URL = "redis://localhost"
REDIS_URL_PRODUCTION = f"redis://{DB_HOST_PRODUCTION}"
TEST_REDIS_URL = f"redis://{TEST_REDIS_URL}"
