from typing import Dict, List
from pydantic import BaseModel
from models.book import Book

flatten = lambda z: [x for y in z for x in y]  # noqa: E731

Options = Dict[str, int]
OptionList = Dict[str, Options]
TitleList = List[str]


class FilterResult(BaseModel):
    books: TitleList
    options: OptionList

    class Config:
        schema_extra = {
            "example": {
                "books": ["Call of the Wild", "1,2,3", "Surely youre joking Mr. Feynman "],
                "options": {
                    "kick": {"real": 2, "funny": 1},
                    "language": {"english": 1, "german": 1, "french": 1}
                }
            }
        }

class BookAndDebug(BaseModel):
    loginfo: str
    book: Book