from utils.db_object import db
import uuid


def next_id() -> str:
    return uuid.uuid1().hex


async def execute(query, is_many, values=None):
    if is_many:
        await db.execute_many(query=query, values=values)
    else:
        await db.execute(query=query, values=values)


async def fetch(query, is_one, values=None):
    if is_one:
        result = await db.fetch_one(query=query, values=values)
        if result is None:
            out = None
        else:
            out = dict(result)
    else:
        result = await db.fetch_all(query=query, values=values)
        if result is None:
            out = None
        else:
            out = []
            for row in result:
                out.append(dict(row))

    return out


# code to test
# query = "insert into books values(:book_id, :title, :hero, :kick, :language, :duration)"
# values = {"book_id": next_id(), "title": "Rox Schulz", "hero": "human", "kick": "real", "language": "german", "duration": "medium"}  # noqa: E501

# loop = asyncio.get_event_loop()
# loop.run_until_complete(execute(query, False, values))
