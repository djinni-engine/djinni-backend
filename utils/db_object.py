from databases import Database
from utils.const import DB_URL, TESTING, TEST_DB_URL, IS_PRODUCTION, DB_URL_PRODUCTION

if TESTING:
    # backend not running
    db = Database(TEST_DB_URL)
elif IS_PRODUCTION:
    db = Database(DB_URL_PRODUCTION)
else:
    # regular dev mode
    db = Database(DB_URL)

async def activate_test_db():
    global db
    db = Database(TEST_DB_URL)
    await db.connect()

async def deactivate_test_db():
    global db
    db = Database(TEST_DB_URL)
    await db.disconnect()

