import aioredis
from utils.const import TESTING, TEST_REDIS_URL, REDIS_URL

redis = None


# async def check_test_redis():
#     global redis
#     if TESTING:
#         redis = await aioredis.create_redis_pool(TEST_REDIS_URL)

async def activate_test_redis():
    global redis
    # redis = await aioredis.create_redis_pool(REDIS_URL)
    redis = await aioredis.create_redis_pool(TEST_REDIS_URL)

async def deactivate_test_redis():
    global redis
    redis.close()
    await redis.wait_closed()


