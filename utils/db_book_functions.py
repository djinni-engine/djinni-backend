from utils.db import fetch, execute
from models.book import Category, Book, ALL_CATEGORIES_NAMES
from typing import List
from test.fixtures import books


async def db_get_book_by_id(book_id: str):
    query = """SELECT * from books WHERE book_id=:book_id"""
    values = {"book_id": book_id}
    result = await fetch(query, True, values)
    return result


async def db_get_all_books_from_category_value(category: Category, value: str):
    query = f"SELECT * from books WHERE {category.value}=:value"
    values = {"value": value}
    result = await fetch(query, False, values)
    return result


async def db_filter_by_categories_and_values(cs: List[Category], vs: List[str]):
    # REFACTOR: switch to using sqlalchemy instead of raw SQL
    outer_query = query_for_category_value(cs[0], vs[0])
    sub_queries = [as_subquery(c, v) for c, v in zip(cs[1:], vs[1:])] if len(cs) > 1 else []
    query = outer_query + "".join(sub_queries)
    result = await fetch(query, False, {})
    return result


async def db_get_options_after_filter(cs: List[Category], vs: List[str]):
    remaining_categories = set(ALL_CATEGORIES_NAMES) - set(cs)
    options = {}
    for c in remaining_categories:
        outer_query = query_for_category_count(c, cs[0], vs[0])
        sub_queries = [as_subquery(c, v) for c, v in zip(cs[1:], vs[1:])] if len(cs) > 1 else []
        query = outer_query + "".join(sub_queries) + f" GROUP BY {c}"
        options_c_result = await fetch(query, False, {})
        options[c] = extract_values_and_counts(c, options_c_result)
    return options


# async def clear_books():
#     query = """ DELETE FROM books """
#     await execute(query=query, is_many=False, values={})


async def db_insert_book(b: Book):
    query = f"""INSERT INTO books(book_id, title, kick, hero, duration, language)
                VALUES ('{b.book_id}', '{b.title}', '{b.kick}', '{b.hero}', '{b.duration}', '{b.language}')"""
    await execute(query=query, is_many=False, values={})


# TODO
# async def db_insert_user(u: User):
#     query = f"""INSERT INTO books(book_id, title, kick, hero, duration, language)
#                 VALUES ('{b.book_id}', '{b.title}', '{b.kick}', '{b.hero}', '{b.duration}', '{b.language}')"""
#     await execute(query=query, is_many=False, values={})


# # DEBUGGING FUNCTION
async def db_insert_fixtures():
    inserted = []
    for b in books.values():
        print("trying to insert", b)
        book = Book(**b)
        try:
            await db_insert_book(book)
            print("inserted", book.title)
            inserted.append(book.title)
        except Exception as e:
            print("ERROR: could not insert ", book.title)
            print(e)
    return inserted


async def db_remove_book(book_id: int):
    query = f"DELETE FROM books WHERE book_id='{book_id}'"
    await execute(query=query, is_many=False, values={})


async def clear_db():
    query = """DELETE FROM books"""
    # query2 = """delete from users;"""
    await execute(query, False)
    # await execute(query2, False)


def query_for_category_count(remaining_c: str, last_c: str, last_v: str):
    return f"SELECT {remaining_c}, COUNT (*) from books WHERE {last_c} = '{last_v}'"


def query_for_category_value(c: str, v: str):
    return f"SELECT title from books WHERE {c} = '{v}'"


def as_subquery(c: str, v: str):
    return " AND title in (" + query_for_category_value(c, v) + ")"


def extract_values_and_counts(category, options_list):
    """ Methods to parses the results from the list of strings returned by SQL
    """
    ret = {}
    for op in options_list:
        value, count = list(op.values())
        ret[value] = count
    return ret
