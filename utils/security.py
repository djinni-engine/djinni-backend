from passlib.context import CryptContext
from starlette.status import HTTP_401_UNAUTHORIZED
from .const import (
    JWT_ALGORITHM,
    JWT_EXPIRATION_TIME_MINUTES,
    JWT_SECRET_KEY,
    JWT_INVALID,
    JWT_EXPIRED_MSG,
    JWT_PERMISSIONS,
)
from models.user import JWTUser

# from utils.db_user_functions import db_check_token_user
from datetime import datetime, timedelta
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
import jwt
import time
from typing import List
from utils.db_user_functions import db_check_token_user

pwd_context = CryptContext(schemes=["bcrypt"])
oauth_schema = OAuth2PasswordBearer(tokenUrl="/token")


def get_hashed_password(password: str):
    return pwd_context.hash(password)


def verify_password(plain_pw: str, hashed_pw: str):
    try:
        return pwd_context.verify(plain_pw, hashed_pw)
    except Exception as e:
        print(e)
        return False


# TEST
# secret = "pass1"
# hash = get_hashed_password(secret)
# print(hash)
# print(verify_password(secret, hash))
# ========================================================================
# ===================== JWT ==============================================
# ========================================================================

# Authenticate username and password
async def authenticate_user(user: JWTUser):
    """ checks whether a user as defined by the jwt is actually in our user database """
    potential_users = await db_check_token_user(user)
    is_valid = False
    for db_user in potential_users:
        if verify_password(user.password, db_user["password"]):
            is_valid = True

    if is_valid:
        user.permissions = []
        return user

    return None


# Create access JWT token
def create_jwt_token(user: JWTUser):
    expiration = datetime.utcnow() + timedelta(minutes=JWT_EXPIRATION_TIME_MINUTES)
    jwt_payload = {"sub": user.username, "permissions": user.permissions, "exp": expiration}
    jwt_token = jwt.encode(jwt_payload, JWT_SECRET_KEY, algorithm=JWT_ALGORITHM)
    return jwt_token


# Check whether JWT token is correct
async def check_jwt_token(token: str = Depends(oauth_schema)):
    try:
        jwt_payload = jwt.decode(token, JWT_SECRET_KEY, algorithms=JWT_ALGORITHM)
        # username = jwt_payload.get("sub")
        permissions = jwt_payload.get("permissions")
        expiration = jwt_payload.get("exp")
        if time.time() < expiration:
            # is_valid = await db_check_jwt_username(username)
            is_valid = True
            if is_valid:
                return final_checks(permissions)
            else:
                raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail=JWT_PERMISSIONS)
        else:
            raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail=JWT_EXPIRED_MSG)
    except Exception:
        # could not decode jwt
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail=JWT_INVALID)


def final_checks(permissions: List[str]):
    """ optional place to do some advanced permission routines """
    return True
